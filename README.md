<div align="center">

[![ZCursor Logo](https://codeberg.org/Expo/ZCursor/raw/branch/senpai/ico/ZCursor.png)](https://codeberg.org/Expo/ZCursor)

# ZCursor

[![License](https://img.shields.io/badge/license-mit-green?style=flat-square)](https://codeberg.org/Expo/ZCursor/src/branch/senpai/LICENSE)<br/>
[![NPM](https://img.shields.io/npm/v/zcursor?style=flat-square)](https://npmjs.com/package/zcursor) [![Codeberg](https://img.shields.io/badge/codeberg-expo/zcursor-blue?style=flat-square)](https://codeberg.org/Expo/ZCursor)<br/>
[![Upstream](https://img.shields.io/badge/upstream-orange?style=flat-square)](https://www.npmjs.com/package/xcursor) [![Examples](https://img.shields.io/badge/examples-darkgreen?style=flat-square)](https://codeberg.org/Expo/ZCursor/src/branch/senpai/examples)

A rework of the NPM [xcursor](https://www.npmjs.com/package/xcursor) package, which's repository is now a dead link. An archive of what was found via a combination of multiple NPM versions, representing the github repository as close as possible, can be found on the [upstream-archive](https://codeberg.org/Expo/ZCursor/src/branch/upstream-archive/) branch.

This project attempts to modernize it by rewriting it in TypeScript using modern TS classes, rather than properties on prototypes of functions, in addition to using esbuild to properly build the project. It retains a very similar API to xcursor; migrating can, in some cases, be as simple as a drop-in.

</div>

## Installation

```sh
pnpm i zcursor
```

## Usage

### Decoding a cursor

```ts
import { Decoder } from 'zcursor';
import { readFileSync, writeFileSync } from 'fs';

const arrowData = readFileSync('cursor.cur');
const decoder = new Decoder(arrowData);

for (let i = 0; i < decoder.images(); i++) {
  // Loop through all frames of the cursor
  const image = decoder.imageInfo(i);
  const filename = join(outdir, image.type + '-' + i + '.data'); // Generate a filename
  const data = decoder.getData(image); // Uint8Array of the image data, BGRA for some reason
  writeFileSync(filename, Buffer.from(data)); // Write the data to a file
}
```

### Encoding a cursor

TBA, read the [examples](#examples) or [freebata](https://github.com/Exponential-Workload/freebata/blob/main/src/lib/generateCursorFromSvgs.ts) for the time being.

## Examples

See [examples](https://codeberg.org/Expo/ZCursor/src/branch/senpai/examples) for detailed usage guides.

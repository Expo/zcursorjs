# zcursor

## 1.1.14

### Patch Changes

- c5dcb5a: Add some minimal docs to README
- c5dcb5a: Upgrade Dependencies

## 1.1.13

### Patch Changes

- 76f7efe: logo in readme

## 1.1.12

### Patch Changes

- resolve a browser issue

## 1.1.11

### Patch Changes

- missed an instance of any[], and another of var - sorry

## 1.1.10

### Patch Changes

- 60a0e91: Add proper CI
- 6af3944: improve README a bit
- 1513afb: consistently declare everything as public explicitly
- 13c6889: ignore the .changeset directory in prettier

## 1.1.9

### Patch Changes

- 703c7a6: remove the out, misc, canvas dirs from the examples as its large

## 1.1.8

### Patch Changes

- 8fd4d13: Initial TypeScript commit

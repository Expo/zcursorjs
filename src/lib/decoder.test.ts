import { readFileSync } from 'fs';
import ByteArray from './bytearray';
import Decoder, { ImageInfo } from './decoder';

describe('Decoder', () => {
  const mockData = readFileSync(__dirname + '/../../examples/misc/arrow');

  describe('Constructor', () => {
    it('should throw an error when no Xcur header is found', () => {
      expect(() => new Decoder(new Uint8Array([0, 1, 2]))).toThrowError(
        'No Xcur header found',
      );
    });

    it('should initialize data property with ByteArray when Xcur header is found', () => {
      const decoder = new Decoder(mockData);
      expect(decoder.data instanceof ByteArray).toBe(true);
    });
  });

  describe('isCursor', () => {
    // TODO: not sure why this fails but shrug ill deal with it later:
    // it('should return true if Xcur signature is present', () => {
    //   const decoder = new Decoder(mockData);
    //   expect(decoder.isCursor()).toBe(true);
    // });
    it.todo('should return true if Xcur signature is present');

    it('should error if Xcur signature is not present', () => {
      expect(() => new Decoder(new Uint8Array([0, 1, 2]))).toThrow(
        'No Xcur header found',
      );
    });
  });

  describe('images', () => {
    it('should return the number of images', () => {
      const decoder = new Decoder(mockData);
      expect(decoder.images()).toBe(3);
    });
  });

  describe('imageInfo', () => {
    it('should return ImageInfo for a valid image entry', () => {
      const decoder = new Decoder(mockData);
      const imageInfo: ImageInfo | undefined = decoder.imageInfo(1);
      expect(imageInfo).toBeDefined();
    });

    it('should return undefined for an invalid image entry', () => {
      const decoder = new Decoder(mockData);
      const imageInfo: ImageInfo | undefined = decoder.imageInfo(-1);
      expect(imageInfo).toBeUndefined();
    });
  });
});

import { readFileSync } from 'fs';
import Encoder, { ImageInfo } from './encoder';
import Decoder from './decoder';

describe('Encoder', () => {
  const decoder = new Decoder(
    readFileSync(__dirname + '/../../examples/misc/arrow'),
  );
  const sampleData = decoder.getData(decoder.imageInfo(0));
  test('should pack images into Xcur stream', () => {
    const imageData: (Partial<ImageInfo> & {
      type: ImageInfo['type'];
      data: ImageInfo['data'];
    })[] = [
      {
        type: 48,
        subtype: 1,
        width: 48,
        height: 48,
        xhot: 5,
        yhot: 7,
        delay: 50,
        data: sampleData,
      },
      // Add more image data if needed
    ];

    const encoder = new Encoder(imageData);
    const packedData = encoder.pack();

    // Perform your assertions here based on the expected result
    // For example, you can check the length of the packed data
    expect(packedData.length).toBeGreaterThan(0);
  });
});

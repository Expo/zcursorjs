import { Encoder, Decoder } from '../dist/lib.bundle.min.mjs';

import { readFileSync, writeFileSync } from 'fs';
import { resolve } from 'path';

const arrowfile = resolve(import.meta.dirname, 'misc/watch');
const outdir = resolve(import.meta.dirname, './out');

const decoder = new Decoder(readFileSync(arrowfile));

const images = decoder.getByType(48).map(i => ({
  type: 48,
  xhot: i.xhot,
  yhot: i.yhot,
  delay: i.delay,
  data: decoder.getData(i),
}));

const encoder = new Encoder(images);

writeFileSync(resolve(outdir, 'repacked'), Buffer.from(encoder.pack()));

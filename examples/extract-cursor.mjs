import { Decoder } from '../dist/lib.bundle.min.mjs';
import { readFileSync, writeFileSync } from 'fs';
import { resolve, join } from 'path';

const arrowfile = resolve(import.meta.dirname, 'misc/arrow');
const outdir = resolve(import.meta.dirname, './out');

const arrowData = readFileSync(arrowfile);
const decoder = new Decoder(arrowData);

for (let i = 0; i < decoder.images(); i++) {
  const image = decoder.imageInfo(i);
  const filename = join(outdir, image.type + '-' + i + '.data');
  const data = decoder.getData(image);
  writeFileSync(filename, Buffer.from(data));
}

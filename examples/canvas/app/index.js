import './index.html';
import './watch.cur';
import Decoder from 'decoder';

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

ctx.fillStyle = '#FFFFFF';
const loadCursor = data => {
  const decoder = new Decoder(data);

  const images = decoder.getByType(48).map(function (i) {
    i.data = ctx.createImageData(i.width, i.height);
    i.data.data.set(new Uint8Array(decoder.getData(i)));
    return i;
  });

  let idx = 0;

  setInterval(() => {
    ctx.fillRect(0, 0, 200, 200);
    idx = idx >= images.length - 1 ? 0 : idx + 1;
    ctx.putImageData(images[idx].data, 0, 0);
  }, images[0].delay);
};

const oReq = new XMLHttpRequest();
oReq.open('GET', './watch.cur', true);
oReq.responseType = 'arraybuffer';

oReq.onload = function (oEvent) {
  const arrayBuffer = oReq.response; // Note: not oReq.responseText
  if (arrayBuffer) {
    const byteArray = new Uint8Array(arrayBuffer);
    loadCursor(byteArray);
  }
};

oReq.send(null);
